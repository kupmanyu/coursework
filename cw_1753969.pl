:- dynamic find_identity/1.

% Accomplish a given Task and return the Cost
solve_task(Task,Cost) :-
    my_agent(A), get_agent_position(A, P),
    get_agent_energy(A, Energy),
    (Task = find(_) ->
        % Find path to object
        solve_task_astar(Task,[0:P:[]],[],[P|Path]), !,
        length(Path, ExpectedCost),
        % Check if expected path can be executed with current energy
        (ExpectedCost > Energy ->
            % Since length of path is greater than energy, attempt to go to charging station
            % before going to object. If no charging station is reachable from current position,
            % the task fails.
            solve_task_astar(find(c(_)), [0:P:[]], [], [P|CPath]), !,
            length(CPath, CCost),
            (CCost > Energy -> say("Cannot complete task", A), !, fail
            ;CCost =< Energy -> agent_do_moves(A, CPath),
                (CPath = [] -> NP = P
                ;otherwise -> reverse(CPath, RCPath), head(RCPath, NP)
                )
            ;otherwise -> say("Cannot complete task", A), !, fail
            ),
            map_adjacent(NP, _, c(X)), agent_topup_energy(A, c(X)),
            say("Topup", A),
            solve_task_astar(Task,[0:NP:[]],[],[NP|Path]), !,
            length(Path, Cost), get_agent_energy(A, NEnergy),
            (Cost is 0 -> say("Cannot complete task", A), !, fail
            ;Cost =< NEnergy -> agent_do_moves(A,Path)
            ;otherwise -> say("Cannot complete task", A), !, fail
            )
        ;otherwise ->
            % Since the cost of the path is less than agent energy, execute the path.
            agent_do_moves(A, Path),
            Cost = ExpectedCost
        )
    ;otherwise ->
        % Since the position of the node to go to is known, we can check if the Manhattan
        % distance between the agent position and destination is greater than the agent energy.
        % If the Manhattan distance is greater, we must first go to a charging station before
        % finding a path to the destination.
        viable(Task), get_task(TaskP, Task),
        lookup_pos(TaskP, empty),
        check_blocked([TaskP:[]], []),
        get_distance(Task, P, Distance),
        (Distance > Energy ->
            solve_task_astar(find(c(_)), [0:P:[]], [], [P|CPath]), !,
            length(CPath, CCost),
            (CCost > Energy -> say("Cannot complete task", A), !, fail
            ;CCost =< Energy -> agent_do_moves(A, CPath),
                (CPath = [] -> NP = P
                ;otherwise -> reverse(CPath, RCPath), head(RCPath, NP)
                )
            ;otherwise -> say("Cannot complete task", A), !, fail
            ),
            map_adjacent(NP, _, c(X)), agent_topup_energy(A, c(X)),
            say("Topup", A),
            solve_task_astar(Task,[0:NP:[]],[],[NP|Path]), !,
            length(Path,Cost), get_agent_energy(A,NEnergy),
            (Cost is 0 -> say("Cannot complete task", A), !, fail
            ;Cost =< NEnergy -> agent_do_moves(A,Path)
            ;otherwise -> say("Cannot complete task", A), !, fail
            )
        ;otherwise ->
            % Since the Manhattan distance between the two is less than the agent energy,
            % we first find a path to the destination, and then verify if it is possible
            % to execute with our current energy. If it is not possible, we must go to a
            % charging station before going to the destination. If no charging stations
            % are reachable from the current location with the current energy, this fails
            solve_task_astar(Task,[0:P:[]],[],[P|Path]), !,
            length(Path,Cost),
            (Cost is 0 -> say("Cannot complete task", A), !, fail
            ;Cost =< Energy -> agent_do_moves(A,Path)
            ;otherwise ->
                  % Since cost of the path is greater than current energy, go to charging
                  % station and then go to destination.
                  solve_task_astar(find(c(_)), [0:P:[]], [], [P|CPath]), !,
                  length(CPath, CCost),
                  (CCost > Energy -> say("Cannot complete task", A), !, fail
                  ;CCost =< Energy -> agent_do_moves(A, CPath),
                      (CPath = [] -> NP = P
                      ;otherwise -> reverse(CPath, RCPath), head(RCPath, NP)
                      )
                  ;otherwise -> say("Cannot complete task", A), !, fail
                  ),
                  map_adjacent(NP, _, c(X)), agent_topup_energy(A, c(X)),
                  say("Topup", A),
                  solve_task_astar(Task,[0:NP:[]],[],[NP|NPath]), !,
                  length(NPath,NCost), get_agent_energy(A,NEnergy),
                  (NCost is 0 -> say("Cannot complete task", A), !, fail
                  ;NCost =< NEnergy -> agent_do_moves(A,NPath)
                  ;otherwise -> say("Cannot complete task", A), !, fail
                  )
            )
        )
    ).

% Calculate the path required to achieve a Task
solve_task_dfs(Task,[P|Ps],Path) :-
    achieved(Task,P), reverse([P|Ps],Path)
    ;
    map_adjacent(P,Q,empty), \+ member(Q,Ps),
    solve_task_dfs(Task,[Q,P|Ps],Path).

% Calculate the path required to achieve a Task using breadth-first search
solve_task_bfs(Task,[Pos:RPath|Queue],Visited,Path) :-
    achieved(Task, Pos), reverse([Pos|RPath], Path)
    ;
    children([Pos:RPath|Queue], Visited, Children),
    append(Queue, Children, NewQueue),
    solve_task_bfs(Task, NewQueue, [Pos|Visited], Path).

%
check_blocked([Pos:RPath|Queue], Visited) :-
    length(Visited, 10)
    ;
    children([Pos:RPath|Queue], Visited, Children),
    append(Queue, Children, NewQueue),
    check_blocked(NewQueue, [Pos|Visited]).

solve_task_astar(Task,[_:Pos:RPath|Queue],Visited,Path) :-
    achieved(Task, Pos), reverse([Pos|RPath], Path)
    ;
    children([Pos:RPath|Queue], Visited, Children),
    (Task=find(_) -> % If task is find(), then perform breadth-first search (equivalent to A* with monotonic function returning
                     % distance between current position and start point)
      append(Queue, Children, NewQueue),
      solve_task_bfs(Task, NewQueue, [Pos|Visited], Path)
    ;otherwise ->    % Otherwise, perform A* search with sorting of agenda based on distance calculated
      maplist(distance(Task), Children, Dists),
      maplist(pack, Dists, Children, ChildrenDistance),
      sort(ChildrenDistance, SortedChildren),
      ord_union(Queue, SortedChildren, NewQueue),
      solve_task_astar(Task, NewQueue, [Pos|Visited], Path)
    ).

% True if the Task is achieved with the agent at Pos
achieved(Task,Pos) :-
    Task=find(Obj), map_adjacent(Pos,_,Obj)
    ;
    Task=go(Pos).

%%%%%%%%%%%%%%%%%%%%% HELPER FUNCTIONS %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find adjacent nodes to current position while excluding previously visited nodes and nodes already in queue
children([Node:Nodes|Queue], Visited, Children) :-
    findall(NP:[Node|Nodes], (map_adjacent(Node, NP, empty), \+ member(NP, Visited), \+ member(NP:_, Queue)), A),
    findall(NP:[Node|Nodes], (map_adjacent(Node, NP, a(_)), \+ member(NP, Visited), \+member(NP:_, Queue)), B),
    append(A, B, Children).

viable(go(P)) :-
    findall(X, map_adjacent(P, X, empty), Xs),
    \+ length(Xs, 0).

viable(find(_)) :- true.

get_distance(go(Target), Pos, Distance) :-
    map_distance(Pos, Target, Distance).

get_distance(find(_), _, 0).

distance(_, _:[], 0).

% Return distance from start to current node + distance from current node to target nodes
% Distance from current node to target node is 0 if the position of the target node is not known
distance(go(Target), Pos:RPath, Distance) :-
    ground(Target), length(RPath, Dist1), map_distance(Pos, Target, Dist2), (Distance is Dist1 + Dist2).

pack(X,Y,X:Y).

point(X,Y,p(X,Y)).

removeAll(_, [], []).
removeAll(X, [X|T], L):- removeAll(X, T, L), !.
removeAll(X, [H|T], [H|L]):- removeAll(X, T, L ).

get_empty(P, Empty) :-
    findall(E, map_adjacent(P, E, empty), Empty).

get_task(p(X,Y), go(p(X,Y))).

head([], []).

head([H|_], H).

tail([_|T], T).

div(L, A, B) :-
    split(L, L, A, B).

split(B, [], [], B).

split([H|T], [_, _|T1], [H | T2], B) :-
    split(T, T1, T2, B).
