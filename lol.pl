% Solve the maze, aiming to get all the agents to p(N,N)
solve_maze :-
    my_agents(Agents),
    setup_lists(Agents, [], List),
    solve_maze(Agents, List, List, [], []).

%solve_maze(Visited, CurrPath) :-
%    my_agents(Agents),
%    find_moves(Agents, Moves, Visited, CurrPath, NewVisited, NewCurrPath),
%    agents_do_moves(Agents, Moves),
%    (achieved(Agents) -> true
%    ;otherwise -> solve_maze(NewVisited, NewCurrPath)
%    ).

solve_maze(Agents, Visited, AgentPaths, DeadEnds, SavePoints) :-
    not_blocked(Agents, [], NewAgents, Visited, DeadEnds),
    find_moves(NewAgents, Moves, Visited, AgentPaths, DeadEnds, SavePoints, NewVisited, NewAgentPaths, NewDeadEnds, NewSavePoints),
    agents_do_moves(NewAgents, Moves),
    (achieved(Agents) -> true
    ;otherwise ->
    solve_maze(Agents, NewVisited, NewAgentPaths, NewDeadEnds, NewSavePoints)
    ).

%%%%%%%%%%%%%%%% USEFUL PREDICATES %%%%%%%%%%%%%%%%%%
% Find a possible move for each agent
find_moves([],[], Visited, CurrPath, DeadEnds, SavePoints, Visited, CurrPath, DeadEnds, SavePoints).

find_moves([A|As], [M|Moves], Visited, AgentPaths, DeadEnds, SavePoints, NewVisited, NewAgentPaths, NewDeadEnds, NewSavePoints) :-
    get_agent_element(A, Visited, AgentVisited),
    get_agent_element(A, AgentPaths, AgentPath),
    findall(P, (agent_adjacent(A, P, empty), \+ check_visited(P, Visited)), PosMoves),
    findall(P, (agent_adjacent(A, P, empty), \+ member(P, AgentVisited), \+ member(P, DeadEnds)), OtherMoves),
    get_agent_position(A, AP),
    (length(PosMoves, 0), length(OtherMoves, 0) ->
        new_path(AP, AgentPath, T),
        (AP = T ->
            head(AgentPath, M), replace_element(A, [AP|AgentVisited], Visited, UpVisited),
            append_deadends([AP|AgentPath], SavePoints, DeadEnds, [], UpDeadEnds),
            find_moves(As, Moves, UpVisited, AgentPaths, UpDeadEnds, SavePoints, NewVisited, NewAgentPaths, NewDeadEnds, NewSavePoints)
        ;otherwise ->
            M = T, delete_one(AP, AgentPath, NewAgentPath), replace_element(A, NewAgentPath, AgentPaths, UpAgentPaths),
            find_moves(As, Moves, Visited, UpAgentPaths, DeadEnds, SavePoints, NewVisited, NewAgentPaths, NewDeadEnds, NewSavePoints)
        )
    ;length(PosMoves, Len), Len > 0 ->
        head(PosMoves, M),
        (member(AP, AgentPath) -> replace_element(A, [AP|AgentVisited], Visited, UpVisited),
            (Len > 1 -> tail(PosMoves, RestMoves), append(RestMoves, SavePoints, UpSavePoints),
                find_moves(As, Moves, UpVisited, AgentPaths, DeadEnds, UpSavePoints, NewVisited, NewAgentPaths, NewDeadEnds, NewSavePoints)
            ;otherwise -> find_moves(As, Moves, UpVisited, AgentPaths, DeadEnds, SavePoints, NewVisited, NewAgentPaths, NewDeadEnds, NewSavePoints))
        ;otherwise -> replace_element(A, [AP|AgentVisited], Visited, UpVisited),
            replace_element(A, [AP|AgentPath], AgentPaths, UpAgentPaths),
            (Len > 1 -> tail(PosMoves, RestMoves), append(RestMoves, SavePoints, UpSavePoints),
                find_moves(As, Moves, UpVisited, UpAgentPaths, DeadEnds, UpSavePoints, NewVisited, NewAgentPaths, NewDeadEnds, NewSavePoints)
            ;otherwise -> find_moves(As, Moves, UpVisited, UpAgentPaths, DeadEnds, SavePoints, NewVisited, NewAgentPaths, NewDeadEnds, NewSavePoints))
    )
    ;otherwise ->
        get_move(OtherMoves, DeadEnds, M),
        (member(AP, AgentPath) -> replace_element(A, [AP|AgentVisited], Visited, UpVisited),
            find_moves(As, Moves, UpVisited, AgentPaths, DeadEnds, SavePoints, NewVisited, NewAgentPaths, NewDeadEnds, NewSavePoints)
        ;otherwise -> replace_element(A, [AP|AgentVisited], Visited, UpVisited),
            replace_element(A, [AP|AgentPath], AgentPaths, UpAgentPaths),
            find_moves(As, Moves, UpVisited, UpAgentPaths, DeadEnds, SavePoints, NewVisited, NewAgentPaths, NewDeadEnds, NewSavePoints)
        )
    ).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

append_deadends([], _, _, Acc, Acc).

append_deadends([A|As], SavePoints, DeadEnds, Acc, NewDeadEnds) :-
    \+ member(A, SavePoints), append([A], Acc, NewAcc),
    append_deadends(As, SavePoints, DeadEnds, NewAcc, NewDeadEnds)
    ;
    append(Acc, DeadEnds, NewDeadEnds).

setup_lists([], Acc, Acc).

setup_lists([A|Agents], Acc, List) :-
    get_agent_position(A, Pos),
    (Pos = p(1,1) -> append([A:[]], Acc, NewAcc),
      setup_lists(Agents, NewAcc, List)
    ;otherwise -> solve_task_bfs(go(p(1,1)), [Pos:[]], [], [Pos|Path]),
      reverse(Path, NewPath),
      append([A:NewPath], Acc, NewAcc),
      setup_lists(Agents, NewAcc, List)).

get_left(X:_, X).
get_right(_:Y, Y).

get_agent_element(A, [E|Elements], AgentElement) :-
    get_left(E, A), get_right(E, AgentElement)
    ;
    get_agent_element(A, Elements, AgentElement).

replace_element(_, _, [], []).

replace_element(A, NewElement, [L|List], [N|NewList]) :-
    get_left(L, A), pack(A, NewElement, N), replace_element(A, NewElement, List, NewList)
    ;
    N = L,
    replace_element(A, NewElement, List, NewList).

check_visited(_, []) :- false.

check_visited(Pos, [V|Visited]) :-
    get_right(V, AgentVisited), member(Pos, AgentVisited)
    ;
    check_visited(Pos, Visited).

not_blocked([], Acc, Acc, _, _).

not_blocked([A|Agents], Acc, NewAgents, Visited, DeadEnds) :-
    get_agent_element(A, Visited, AgentVisited),
    findall(P, (agent_adjacent(A, P, empty), \+ check_visited(P, Visited), \+ member(P, DeadEnds)), PosMoves),
    findall(P, (agent_adjacent(A, P, empty), \+ member(P, AgentVisited), \+ member(P, DeadEnds)), OtherMoves),
    findall(P, (agent_adjacent(A, P, a(_)), \+ member(P, AgentVisited), \+ member(P, DeadEnds)), AdjAgents),
    (length(PosMoves, 0), length(OtherMoves, 0), \+ length(AdjAgents, 0) -> not_blocked(Agents, Acc, NewAgents, Visited, DeadEnds)
    ;otherwise -> not_blocked(Agents, [A|Acc], NewAgents, Visited, DeadEnds)).

get_move([M], _, M).

get_move([M|Moves], DeadEnds, Move) :-
    \+ member(M, DeadEnds), Move = M
    ;
    get_move(Moves, DeadEnds, Move).

new_path(Pos, [], Pos).

new_path(Pos, [V|Visited], Move) :-
    (Pos = V -> head(Visited, Move)
    ;otherwise -> new_path(Pos, Visited, Move)).

delete_one(_, [], []).
delete_one(Term, [Term|Tail], Tail) :- !.
delete_one(Term, [Head|Tail], [Head|Result]) :-
    delete_one(Term, Tail, Result).

achieved([]) :- false.

achieved([A|Agents]) :-
    achieved(A), leave_maze(A)
    ;
    achieved(Agents).

achieved(A) :-
    ailp_grid_size(Size),
    get_agent_position(A, P),
    P = p(Size,Size).
