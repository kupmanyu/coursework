% True if link L appears on A's wikipedia page
actor_has_link(L,A) :-
    actor(A), wp(A,WT), wt_link(WT,L).

% Greedy approach, find closest unvisited oracle and move adjacent to it.
% After a move and query is done, check if the energy level of the agent
% and check if the energy level is below a certain thershold. If it is
% below the threshold, move to charging station and charge the agent.
% In case all oracles have been visited, any further attempts searching
% for an oracle will fail, resulting in the identity being unknown.
% Otherwise, the list of possible identities is narrowed down until we get
% the answer.
eliminate(As,A,V) :-
    As=[A], !
    ;
    V is 10, !, A = unknown
    ;
    (solve_find(find(o(_)), K) ->
        !,
        my_agent(N),
        agent_ask_oracle(N,o(K),link,L),
        include(actor_has_link(L),As,ViableAs),
        check_energy(N),
        V1 is V+1
    ;otherwise -> ViableAs = As, V1 is 10
    ),
    eliminate(ViableAs,A,V1).

% Deduce the identity of the secret actor A
find_identity(A) :-
    my_agent(N), check_energy(N),
    findall(A,actor(A),As), eliminate(As,A,0).

% Slightly modified version of solve_task from Part 1, which returns the oracle ID
% instead of the cost. This function only takes in a Task of type find(o(_)).
solve_find(Task, K) :-
    my_agent(A), get_agent_position(A, P),
    get_agent_energy(A, Energy),
    \+ Energy is 0,
    find_object(A, Task, [P:[]],[],[P|Path]), !,
    length(Path, ExpectedCost),
    % Check if expected path can be executed with current energy
    (ExpectedCost > Energy ->
        % Since length of path is greater than energy, attempt to go to charging station
        % before going to object. If no charging station is reachable from current position,
        % the task fails.
        find_object(A, find(c(_)), [P:[]], [], [P|CPath]), !,
        length(CPath, CCost),
        (CCost > Energy -> say("Cannot complete task", A), !, fail
        ;CCost =< Energy ->
            (CPath = [] -> NP = P
            ;otherwise -> agent_do_moves(A, CPath), reverse(CPath, RCPath), head(RCPath, NP)
            )
        ;otherwise -> say("Cannot complete task", A), !, fail
        ),
        map_adjacent(NP, _, c(X)), agent_topup_energy(A, c(X)),
        say("Topup", A),
        find_object(A, Task,[NP:[]],[],[NP|Path]), !,
        length(Path, Cost), get_agent_energy(A, NEnergy),
        (Cost is 0 -> say("Cannot complete task", A), !, fail
        ;Cost =< NEnergy ->
            agent_do_moves(A,Path),
            (Path = [] ->
                map_adjacent(P, _, o(K))
            ;otherwise ->
                reverse(Path, RPath), head(RPath, OP), map_adjacent(OP, _, o(K))
            )
        ;otherwise -> say("Cannot complete task", A), !, fail
        )
    ;otherwise ->
        % Since the cost of the path is less than agent energy, execute the path.
        agent_do_moves(A, Path),
        (Path = [] ->
            map_adjacent(P, _, o(K))
        ;otherwise ->
            reverse(Path, RPath), head(RPath, OP), map_adjacent(OP, _, o(K))
        )
    ).

% Slightly modified version of solve_task_bfs, which the addition of the Agent input
% to test if the agent has already visited the Oracle when calling achieved. Since the
% the Task is a find task, we use the bfs search instead of an A*, as the location of
% the destination is not known.
find_object(Agent, Task, [Pos:RPath|Queue], Visited, Path) :-
    achieved(Agent, Task, Pos), reverse([Pos|RPath], Path)
    ;
    children([Pos:RPath|Queue], Visited, Children),
    append(Queue, Children, NewQueue),
    find_object(Agent, Task, NewQueue, [Pos|Visited], Path).

% Modified achieved function from Part 1, where it when the agent is adjacent to
% an unvisited oracle when the task if to find an oracle, or when it is adjacent
% to a charging station when the task is to find a charging station
achieved(Agent, Task, Pos) :-
    Task = find(o(_)),
    map_adjacent(Pos, _, o(X)),
    \+ agent_check_oracle(Agent, o(X))
    ;
    Task = find(c(_)),
    map_adjacent(Pos, _, c(_)).

% Check energy of agent A, if the energy is less than Emin, then move to the nearest
% charging station and charge the agent. 
check_energy(A) :-
    ailp_grid_size(Size),
    Emax is (Size * Size) / 4,
    Emin is (Emax / 4),
    get_agent_energy(A, Energy),
    get_agent_position(A, P),
    (Energy < Emin ->
        find_object(A, find(c(_)), [P:[]], [], [P|Path]),
        agent_do_moves(A, Path),
        (Path = [] ->
            map_adjacent(P, _, c(X)),
            agent_topup_energy(A, c(X)),
            say("Topup", A)
        ;otherwise ->
            reverse(Path, RPath), head(RPath, NP), map_adjacent(NP, _, c(X)),
            map_adjacent(NP, _, c(X)),
            agent_topup_energy(A, c(X)),
            say("Topup", A)
        )
    ;otherwise -> true
    ).
