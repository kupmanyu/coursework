% Solve the maze, aiming to get all the agents to p(N,N)
solve_maze :-
    my_agents(Agents),
    setup_visit(Agents, [], L),
    solve_maze(Agents, L, L).

solve_maze(Agents, Visited, Path) :-
    find_moves(Agents, Moves, Visited, Path, NewVisited, NewPath),
    agents_do_moves(Agents, Moves),
    %(achieved(Agents) -> true
    %;otherwise ->
    solve_maze(Agents, NewVisited, NewPath).
    %).

%%%%%%%%%%%%%%%% USEFUL PREDICATES %%%%%%%%%%%%%%%%%%
% Find a possible move for each agent
find_moves([], [], V, P, V, P).
find_moves([A|As],[M|Moves], Visited, Path, NewVisited, NewPath) :-
    get_list(A, Visited, AgentVisited),
    get_list(A, Path, AgentPath),
    findall(P,(agent_adjacent(A,P,empty), \+ member(P, AgentVisited)),PosMoves),
    length(AgentPath, PathLen), length(PosMoves, MoveLen),
    (PosMoves = [] ->
        (PathLen > 1 ->
            tail(AgentPath, Temp),
            head(Temp, X),
            (lookup_pos(X, empty) ->
                M = X, tail(AgentPath, T),
                replace_list(A, T, Path, UpPath),
                find_moves(As, Moves, Visited, UpPath, NewVisited, NewPath)
            ;otherwise -> M = [], find_moves(As, Moves, Visited, Path, NewVisited, NewPath))
        ;otherwise ->
            head(AgentPath, M),
            find_moves(As, Moves, Visited, Path, NewVisited, NewPath)
        )
    ;MoveLen is 1 -> head(PosMoves, M),
        replace_list(A, [M|AgentVisited], Visited, UpVisited),
        replace_list(A, [M|AgentPath], Path, UpPath),
        find_moves(As, Moves, UpVisited, UpPath, NewVisited, NewPath)
    ;otherwise -> head(PosMoves, M),
        replace_list(A, [M|AgentVisited], Visited, UpVisited),
        replace_list(A, [M|AgentPath], Path, UpPath),
        find_moves(As, Moves, UpVisited, UpPath, NewVisited, NewPath)
    ).

ins(I, -, t(I, -, -, -)).

ins(I, t(X, L, M, R), t(Y, P, N, Q)) :-
    (L = - -> ins(I, L, U), (Y, P, N, Q) = (X, U, M, R)
    ;M = - -> ins(I, M, U), (Y, P, N, Q) = (X, L, U, R)
    ;R = - -> ins(I, R, U), (Y, P, N, Q) = (X, L, M, U)
    ;otherwise -> fail
    ).

find(X, t(X, L, M, R), t(X, L, M, R)) :- !.

find(I, t(_, L, M, R), T) :-
    find(I, L, T)
    ;
    find(I, M, T)
    ;
    find(I, R, T)
    ;
    fail.

find_parent(X, t(I, t(X, CL, CM, CR), M, R), t(I, t(X, CL, CM, CR), M, R)) :- !.

find_parent(X, t(I, L, t(X, CL, CM, CR), R), t(I, L, t(X, CL, CM, CR), R)) :- !.

find_parent(X, t(I, L, M, t(X, CL, CM, CR)), t(I, L, M, t(X, CL, CM, CR))) :- !.

find_parent(I, t(_, L, M, R), T) :-
    find_parent(I, L, T)
    ;
    find_parent(I, M, T)
    ;
    find_parent(I, R, T)
    ;
    fail.

replace_value(I, X, t(I, L, M, R), t(X, L, M, R)) :- !.

replace_value(I, X, T, N) :-
    find_parent(I, T, t(V, L, M, R)),
    (find(I, L, t(I, CL, CM, CR)) -> !, N = t(V, t(X, CL, CM, CR), M, R)
    ;find(I, M, t(I, CL, CM, CR)) -> !, N = t(V, L, t(X, CL, CM, CR), R)
    ;find(I, R, t(I, CL, CM, CR)) -> !, N = t(V, L, M, t(X, CL, CM, CR))
    ;otherwise -> fail
    ).

delete_subtree(S, T, N) :-
    find(X, S, S),
    find_parent(X, T, t(V, L, M, R)),
    (S = L -> !, N = t(V, -, M, R)
    ;S = M -> !, N = t(V, L, -, R)
    ;S = R -> !, N = t(V, L, M, -)
    ;otherwise -> N = T
    ).

setup_visit([], Acc, Acc).

setup_visit([A|As], Acc, List) :-
    get_agent_position(A, AP),
    append([A:[AP]], Acc, NewAcc),
    setup_visit(As, NewAcc, List).

get_left(X:_, X).
get_right(_:Y, Y).

get_list(A, [L|Ls], AgentList) :-
    get_left(L, A), get_right(L, AgentList)
    ;
    get_list(A, Ls, AgentList).

replace_list(_, _, [], []).

replace_list(A, List, [L|Ls], [N|Ns]) :-
    get_left(L, A), N = A:List, replace_list(A, List, Ls, Ns)
    ;
    N = L, replace_list(A, List, Ls, Ns).

achieved([]) :- false.

achieved([A|Agents]) :-
    achieved(A), leave_maze(A)
    ;
    achieved(Agents).

achieved(A) :-
    ailp_grid_size(Size),
    get_agent_position(A, P),
    P = p(Size,Size).
