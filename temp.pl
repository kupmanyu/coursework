% Solve the maze, aiming to get all the agents to p(N,N)
solve_maze :-
    my_agents(Agents),
    %reverse(Agents, RAgents),
    maplist(create_follow_list, Agents, F),
    setup_follows(Agents, F, Follows),
    maplist(get_agent_position, Agents, Visited),
    start(Agents,Visited,[],Follows).

start(_, 0, _, _) :- true.

start(Agents,Visited,SavePoints,Follows) :-
    find_moves(Agents, Moves, Visited, SavePoints, Follows, NV, NSP, NF),
    %(NV is 0 -> true
    %;otherwise ->
    shorten(Agents, Moves, NF, NewAgents, NewMoves),
    agents_do_moves(NewAgents, NewMoves),
    start(NewAgents, NV, NSP, NF).

shorten(Agents, Moves, NF, NewAgents, NewMoves) :-
    maplist(pack, Agents, Moves, N),
    move_following(Agents, N, NF, [], [], Y),
    maplist(get_agent, Y, NewAgents),
    maplist(get_move, Y, NewMoves).

%%%%%%%%%%%%%%%% USEFUL PREDICATES %%%%%%%%%%%%%%%%%%
% Find a possible move for each agent
find_moves([],[],_).
find_moves([A|As],Visited,SavePoints) :-
    achieved(A), leave_maze(A), find_moves(As, [], []);
    findall(P,(agent_adjacent(A,P,empty), \+ member(P, Visited)),PosMoves),
    length(PosMoves, NumberOfChoices),
    (NumberOfChoices is 0 -> head(SavePoints,SavePoint), tail(SavePoints, NewSavePoints),
      solve(A, go(SavePoint)), find_moves([A|As], Visited, NewSavePoints)
    ;NumberOfChoices is 1 -> get_agent_position(A, AP), head(PosMoves, Move),
      agent_do_moves(A, [Move]), find_moves([A|As],[AP|Visited],SavePoints)
    ;otherwise -> get_agent_position(A, AP), head(PosMoves, Choice1), tail(PosMoves, RestChoices),
      agent_do_moves(A, [Choice1]), append(RestChoices, SavePoints, NewSavePoints),
      find_moves([A|As], [AP|Visited], NewSavePoints)).

find_moves([], [], Visited, SavePoints, Follows, Visited, SavePoints, Follows).

find_moves([A|As],[Move|Moves],Visited,SavePoints,Follows,NV,NSP,NF) :-
    achieved(A), leave_maze(A), my_agents(Agents), removeAll(A, Agents, NewAgents),
    finish(NewAgents), find_moves([], [], 0, 0, 0, NV, NSP, NF);
    is_following(A, Follows, A), get_agent_position(A, AP),
    findall(P,(agent_adjacent(A,P,empty), \+ member(P, Visited)),PosMoves),
    findall(N, agent_adjacent(A, N, empty), Ns),
    length(PosMoves, NumberOfChoices), length(Ns, Test),
    (NumberOfChoices is 0, \+ Test is 0 -> get_end(A, Follows, X), find_savepoint(X, SavePoints, AgentSPs),
      head(AgentSPs, SavePoint),
      (SavePoint = [] -> get_empty(AP, Visited, Pos, Move), append([A:[Pos]], SavePoints, NewSavePoints),
        find_moves(As, Moves, [AP|Visited], NewSavePoints, Follows, NV, NSP, NF)
      ;otherwise -> solve(A, go(SavePoint), Move),
        (Move = SavePoint -> removeAll(A:[SavePoint], SavePoints, NewSavePoints)
        ;otherwise -> NewSavePoints = SavePoints),
        find_moves(As, Moves, [AP|Visited], NewSavePoints, Follows, NV, NSP, NF)
      )
    ;NumberOfChoices is 0 -> Move = "Placeholder", get_end(A, Follows, X), reverse_train(X, Follows, Follows, NewFollows),
      find_moves(As, Moves, [AP|Visited], SavePoints, NewFollows, NV, NSP, NF)
    ;NumberOfChoices is 1 -> head(PosMoves, Move), find_moves(As, Moves, [AP|Visited], SavePoints, Follows, NV, NSP, NF)
    ;otherwise -> head(PosMoves, Move), tail(PosMoves, RestChoices), append([A:RestChoices], SavePoints, NewSavePoints),
      find_moves(As, Moves, [AP|Visited], NewSavePoints, Follows, NV, NSP, NF)
    )
    ;
    findall(P,(agent_adjacent(A,P,empty), \+ member(P, Visited)),PosMoves),
    length(PosMoves, NumberOfChoices),
    (NumberOfChoices is 0 -> Move = "Placeholder",
      find_moves(As, Moves, Visited, SavePoints, Follows, NV, NSP, NF)
    ;otherwise -> head(PosMoves, Move), is_following(A, Follows, X), replace_follow(X, 0, Follows, [], NewFollows),
      find_moves(As, Moves, Visited, SavePoints, NewFollows, NV, NSP, NF)).

finish([]).

finish(As) :-
    ailp_grid_size(Size),
    go_to_finish(go(p(Size, Size)), As, Moves, As, NewAs),
    agents_do_moves(As, Moves),
    finish(NewAs).

go_to_finish(_, [], [], Acc, Acc).

go_to_finish(Task, [A|As], [Move|Moves], Acc, NewAs) :-
    achieved(A), leave_maze(A), removeAll(A, Acc, UpAs), go_to_finish(Task, As, Moves, UpAs, NewAs)
    ;
    solve(A, Task, Move), go_to_finish(Task, As, Move, Acc, NewAs).

find_savepoint(_, [], []).

find_savepoint(A, [A:SavePoint|_], SavePoint).

find_savepoint(A, [_:_|SavePoints], SavePoint) :-
    find_savepoint(A, SavePoints, SavePoint).

solve_task_bfs_empty(MazeVisited,[Pos:RPath|Queue],Visited,Path) :-
    \+ member(Pos, MazeVisited), lookup_pos(Pos, empty), reverse([Pos|RPath], Path)
    ;
    children([Pos:RPath|Queue], Visited, Children),
    append(Queue, Children, NewQueue),
    solve_task_bfs_empty(MazeVisited, NewQueue, [Pos|Visited], Path).

get_empty(AP, MazeVisited, Pos, Move) :-
    solve_task_bfs_empty(MazeVisited, [AP:[]], [], [AP|Path]),
    head(Path, Move),
    last(Path, Pos).

solve(A,Task) :-
    get_agent_position(A, Pos),
    solve_task_bfs(Task, [Pos:[]], [], [Pos|Path]),
    agent_do_moves(A, Path).

solve(A,Task, Move) :-
    get_agent_position(A, Pos),
    solve_task_bfs(Task, [Pos:[]], [], [Pos|Path]),
    head(Path, Move).

create_follow_list(A, A:0).

setup_follows([], Fs, Fs).

setup_follows([A|As], Fs, NFs) :-
    findall(P,agent_adjacent(A,P,empty),PosMoves),
    findall(N, agent_adjacent(A,_,a(N)), Ns),
    length(PosMoves, NumberOfChoices),
    (NumberOfChoices is 0 -> head(Ns, X), replace_follow(X, A, Fs, [], NewFs),
      setup_follows(As, NewFs, NFs)
    ;otherwise -> setup_follows(As, Fs, NFs)).

replace_follow(_, _, [], Acc, N) :-
    reverse(Acc, N).

replace_follow(A, B, [X:Y|Xs], Acc, N) :-
    (A is X -> append([A:B], Acc, NewAcc), replace_follow(A, B, Xs, NewAcc, N)
    ;otherwise -> append([X:Y], Acc, NewAcc), replace_follow(A, B, Xs, NewAcc, N)).

find_follow(A, [A:Ans|_], Ans).

find_follow(X, [_:_|As], Ans) :-
    find_follow(X, As, Ans).

is_following(A, [], A).

is_following(A, [N:F|Ns], Ans) :-
    (A is F -> Ans is N
    ;otherwise -> is_following(A, Ns, Ans)).

reverse_train(A, Follows, Acc, NewFollows) :-
    is_following(A, Follows, X),
    (A is X -> replace_follow(A, 0, Acc, [], NewFollows)
    ;otherwise -> replace_follow(A, X, Acc, [], F), reverse_train(X, Follows, F, NewFollows)
    ).

get_end(A, Follows, N) :-
    (find_follow(A, Follows, 0) -> N is A
    ;otherwise -> find_follow(A, Follows, X), get_end(X, Follows, N)).

get_move(_:Y, Y).

get_agent(X:_, X).

check_moves(A, [A:_|_]).

check_moves(A, [_:_|Xs]) :-
    check_moves(A, Xs).

move_following([], _, _, [], Acc, Moves) :-
    reverse(Acc, Moves).

move_following([], [], Follows, Waitlist, Acc, Moves) :-
    maplist(get_agent, Waitlist, Agents),
    move_following(Agents, Waitlist, Follows, [], Acc, Moves).

move_following([A|As], [OMove|OMoves], Follows, Waitlist, Acc, Moves) :-
    get_move(OMove, Move),
    (\+ Move = "Placeholder" -> append([OMove], Acc, NewAcc),
      move_following(As, OMoves, Follows, Waitlist, NewAcc, Moves)
    ;otherwise ->
      is_following(A, Follows, N),
      (check_moves(N, Acc) -> get_agent_position(N, Pos), append([A:Pos], Acc, NewAcc),
        removeAll(A, Waitlist, NewWaitlist),
        move_following(As, OMoves, Follows, NewWaitlist, NewAcc, Moves)
      ;otherwise -> append([A:Move], Waitlist, NewWaitlist),
        move_following(As, OMoves, Follows, NewWaitlist, Acc, Moves))).

achieved(A) :-
    ailp_grid_size(Size),
    get_agent_position(A, P),
    P = p(Size,Size).
