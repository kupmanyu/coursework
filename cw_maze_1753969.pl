% Solve the maze, aiming to get all the agents to p(N,N)
% Several lists are initialized before calling the second solve_maze function
% with the initialized lists as parameters. The purpose of these lists are:
% Visited   -   A list of all nodes that have been visited by at least 1 agent.
% Path      -   The path of the agent from p(1,1) inclusive to its current location
%               If the agent is initialized at any point not equal to p(1,1), a path
%               from p(1,1) to the current location is simulated and added to the path
% SavePoint -   This list contains all nodes with more than 2 adjacent empty nodes
%               when the grid is instantiated. If an agent is currently occupying
%               a node, it is considered as empty as well. If the current position
%               is p(1,1), the the node is added if the number of adjacent empty
%               nodes is 2.
% Branch    -   The path of the agent from the most recent SavePoint to its current
%               location, excluding the SavePoint.
solve_maze :-
    my_agents(Agents),
    update_visited(Agents, [], Visited),
    setup_lists(Agents, [], Path),
    setup_sp(Agents, [], SP),
    setup_branch(Agents, Path, SP, Branch),
    solve_maze(Agents, Visited, Path, Branch, []).

% Solves the maze by moving all agents till a single agent finds the path to solution.
% Once a solution is found, move all agents onto the path and move them to solution.
% The moves are done by checking if an agent has an empty node adjacent to it. Otherwise,
% the agent does not move.
solve_maze(Agents, Visited, Path, Branch, DeadEnds) :-
   blocked(Agents, [], As),
   find_moves(As, Moves, Visited),
   check_empty_moves(As, Moves, [], NewAs),
   get_non_empty(As, Moves, [], [], NotEmptyAs, NotEmptyMoves),
   follow_moves(NewAs, FollowMoves, Path, DeadEnds, TempPath),
   append(NotEmptyAs, NewAs, MoveAs),
   append(NotEmptyMoves, FollowMoves, AllMoves),
   agents_do_moves(MoveAs, AllMoves),
   update_all(MoveAs, Visited, TempPath, Branch, DeadEnds, NewVisited, NewPath, NewBranch, NewDeadEnds),
   (\+ maze_achieved(Agents, NewPath) ->
     solve_maze(Agents, NewVisited, NewPath, NewBranch, NewDeadEnds)
   ;otherwise -> true).

% Updates all the list to include the moves made
update_all(As, Visited, Path, Branch, DeadEnds, NewVisited, NewPath, NewBranch, NewDeadEnds) :-
    update_visited(As, Visited, NewVisited),
    update_path(As, Path, NewPath),
    update_branch(As, Branch, NewBranch),
    update_deadend(As, NewBranch, DeadEnds, NewDeadEnds).

%%%%%%%%%%%%%%%% USEFUL PREDICATES %%%%%%%%%%%%%%%%%%
% Find a possible move for each agent, excluding nodes that have been visited to
% ensure that agents explore a new path if it is possible.
find_moves([], [], _).

find_moves([A|As], [M|Moves], Visited) :-
    findall(P, (agent_adjacent(A, P, empty), \+ member(P, Visited)), PosMoves),
    head(PosMoves, M),
    find_moves(As, Moves, Visited).

% In the case that the agent can only move onto a visited node, move to a node that
% has not been visited by the current agent, and one that doesn't lead to a dead end
% A dead end is a series of moves that will eventually lead to the agent not reaching
% the goal due to it being stopped by obstacles. In case there are no possible moves
% due to the agent being at the dead end, trace back the path to the save point one
% move at a time.
follow_moves([], [], Visited, _, Visited).

follow_moves([A|As], [M|Moves], Visited, DeadEnds, NewVisited) :-
    get_agent_element(A, Visited, AgentVisited),
    findall(P, (agent_adjacent(A, P, empty), \+ member(P, AgentVisited), \+ member(P, DeadEnds)), PosMoves),
    (PosMoves = [] ->
        second_elm(AgentVisited, M),
        tail(AgentVisited, T),
        replace_element(A, T, Visited, NewAcc),
        follow_moves(As, Moves, NewAcc, DeadEnds, NewVisited)
    ;otherwise -> random_member(M, PosMoves),
        follow_moves(As, Moves, Visited, DeadEnds, NewVisited)
    ).

second_elm(As, A) :-
    tail(As, X), head(X, A).

% Setup Path list as specified in solve_maze
setup_lists([], Acc, List) :-
    reverse(Acc, List).

setup_lists([A|Agents], Acc, List) :-
    get_agent_position(A, Pos),
    (Pos = p(1,1) -> append([A:[p(1, 1)]], Acc, NewAcc),
      setup_lists(Agents, NewAcc, List)
    ;otherwise -> solve_task_bfs(go(p(1,1)), [Pos:[]], [], Path),
      append([A:Path], Acc, NewAcc),
      setup_lists(Agents, NewAcc, List)).

% Setup a list of SavePoints as specified by solve_maze.
setup_sp([], Acc, Acc).

setup_sp([A|As], Acc, SP) :-
    findall(P, agent_adjacent(A, P, empty), Empty),
    findall(P, agent_adjacent(A, P, a(_)), Agents),
    length(Empty, X), length(Agents, Y),
    NumberOfChoices is X + Y,
    get_agent_position(A, AP),
    (AP = p(1,1), NumberOfChoices is 2 -> setup_sp(As, [AP|Acc], SP)
    ;NumberOfChoices > 2 -> setup_sp(As, [AP|Acc], SP)
    ;otherwise -> setup_sp(As, Acc, SP)
    ).

% Helper functions to skip till the most recent SavePoint when setting up Branch
% as specified by solve_maze.
skip_till_sp([], _, _, -1).

skip_till_sp([P|Ps], S, Acc, Branch) :-
    (P = S -> Branch = Acc
    ;otherwise -> skip_till_sp(Ps, S, [P|Acc], Branch)
    ).

find_branch(_, [], -1).

find_branch(P, [S|SP], Branch) :-
    find_branch(P, SP, X),
    (X is -1 -> skip_till_sp(P, S, [], Branch)
    ;otherwise -> Branch = X
    ).

% Setup the branch as specified in solve_maze.
setup_branch(_, X, [], X).

setup_branch([], [], _, []).

setup_branch([A|Agents], [_:P|Path], SP, [B|Branch]) :-
    get_agent_position(A, AP),
    (member(AP, SP) -> B = A:[], setup_branch(Agents, Path, SP, Branch)
    ;otherwise -> find_branch(P, SP, X),
        (X is -1 -> B = A:P, setup_branch(Agents, Path, SP, Branch)
        ;otherwise -> reverse(X, Y), B = A:Y, setup_branch(Agents, Path, SP, Branch))
    ).

% Check if agent has any empty nodes adjacent to it, and return a list of all the
% agents that match this condition.
blocked([], Acc, Acc).

blocked([A|As], Acc, Ns) :-
    findall(P, agent_adjacent(A, P, empty), PosMoves),
    (PosMoves = [] -> blocked(As, Acc, Ns)
    ;otherwise -> blocked(As, [A|Acc], Ns)
    ).

% Check if the list of moves contain the empty list, and return a list of agents
% with the empty list as their moves
check_empty_moves([], [], Acc, Acc).

check_empty_moves([A|As], [M|Ms], Acc, NewAs) :-
    (M = [] -> check_empty_moves(As, Ms, [A|Acc], NewAs)
    ;otherwise -> check_empty_moves(As, Ms, Acc, NewAs)
    ).

% Get a list of all agents that have a move which is not equal to the empty list
get_non_empty([], [], Acc, Acc2, NewAs, NewMoves) :-
    reverse(Acc, NewAs),
    reverse(Acc2, NewMoves).

get_non_empty([A|As], [M|Ms], Acc, Acc2, NewAs, NewMoves) :-
    (M = [] -> get_non_empty(As, Ms, Acc, Acc2, NewAs, NewMoves)
    ;otherwise -> get_non_empty(As, Ms, [A|Acc], [M|Acc2], NewAs, NewMoves)
    ).

get_left(X:_, X).
get_right(_:Y, Y).

get_agent_element(A, [E|Elements], AgentElement) :-
    get_left(E, A), get_right(E, AgentElement)
    ;
    get_agent_element(A, Elements, AgentElement).

% Update the visited list by adding the current location of the agents to it
% if the list does not contain the current location
update_visited([], Visited, Visited).

update_visited([A|As], Visited, NewVisited) :-
    get_agent_position(A, AP),
    (\+ member(AP, Visited) -> update_visited(As, [AP|Visited], NewVisited)
    ;otherwise -> update_visited(As, Visited, NewVisited)
    ).

% Update path of the agent with the current location of the agent if the list
% does not contain the current location.
update_path([], Acc, Acc).

update_path([A|As], Path, NewPath) :-
    get_agent_element(A, Path, AgentPath),
    get_agent_position(A, AP),
    (\+ member(AP, AgentPath) ->
        replace_element(A, [AP|AgentPath], Path, NewAcc),
        update_path(As, NewAcc, NewPath)
    ;otherwise -> update_path(As, Path, NewPath)
    ).

% Update branch by checking if the current location matches the conditions specified
% in solve_maze. If it does, set the branch to the empty list, else append the current
% position of the agent to the branch
update_branch([], Acc, Acc).

update_branch([A|As], Branch, NewBranch) :-
    get_agent_element(A, Branch, AgentBranch),
    findall(P, agent_adjacent(A, P, empty), Empty),
    findall(P, agent_adjacent(A, P, a(_)), Agents),
    length(Empty, X), length(Agents, Y),
    NumberOfChoices is X + Y,
    get_agent_position(A, AP),
    (AP = p(1, 1), NumberOfChoices is 2 ->
        replace_element(A, [], Branch, NewAcc),
        update_branch(As, NewAcc, NewBranch)
    ;NumberOfChoices > 2 ->
        replace_element(A, [], Branch, NewAcc),
        update_branch(As, NewAcc, NewBranch)
    ;otherwise ->
        (\+ member(AP, AgentBranch) ->
            replace_element(A, [AP|AgentBranch], Branch, NewAcc),
            update_branch(As, NewAcc, NewBranch)
        ;otherwise ->
            update_branch(As, Branch, NewBranch)
        )
    ).

% Check if the the current position of the agent has only 1 empty location that is
% not a dead end. If it is, the branch of the agent is added to the list of dead ends
% and the new list is returned. If the current location is a SavePoint and all possible
% nodes from the SavePoint lead to dead ends, add the SavePoint to the list of dead ends
% as well
update_deadend([], _, Acc, Acc).

update_deadend([A|As], Branch, DeadEnds, NewDeadEnds) :-
    get_agent_element(A, Branch, AgentBranch),
    findall(P, (agent_adjacent(A, P, empty), \+ member(P, DeadEnds)), Empty),
    findall(P, (agent_adjacent(A, P, a(_)), \+ member(P, DeadEnds)), Agents),
    length(Empty, X), length(Agents, Y),
    NumberOfChoices is X + Y,
    get_agent_position(A, AP),
    (NumberOfChoices is 1, \+ AP = p(1,1) ->
        (\+ member(AP, DeadEnds) ->
          (AgentBranch = [] -> append([AP], DeadEnds, NewAcc)
          ;otherwise -> append(AgentBranch, DeadEnds, NewAcc)),
          update_deadend(As, Branch, NewAcc, NewDeadEnds)
        ;otherwise -> update_deadend(As, Branch, DeadEnds, NewDeadEnds))
    ;otherwise -> update_deadend(As, Branch, DeadEnds, NewDeadEnds)
    ).

% Replace the element of agent A with a new element while leaving the rest of the
% list unchanged, and return the updated list
replace_element(_, _, [], []).

replace_element(A, NewElement, [L|List], [N|NewList]) :-
    get_left(L, A), pack(A, NewElement, N), replace_element(A, NewElement, List, NewList)
    ;
    N = L,
    replace_element(A, NewElement, List, NewList).

% Skip through the list until the head of the list matched the given value and
% return the tail of the list at that point. If the list does not have the given
% value, return the empty list.
skip_till_current(_, [], []).

skip_till_current(SP, [SP|Xs], Xs).

skip_till_current(SP, [_|Xs], List) :-
    skip_till_current(SP, Xs, List).

% Get the move of agent when the path to solution is known. When the current location
% of the agent is in the path to the solution, get the next move in solution. Else,
% move the agent back along its current path until the agent is on the path to the
% solution. This method succeeds due to all agents starting from the origin p(1,1),
% so any solution found by another agent must be reachable by all other agents, so
% all agents must have a point in their path that is in the path to the solution.
get_finish_move([], [], _, _).

get_finish_move([A|As], [M|Moves], PathToEnd, Path) :-
    get_agent_element(A, Path, AgentPath),
    get_agent_position(A, AP),
    skip_till_current(AP, PathToEnd, List),
    (List = [] -> second_elm(AgentPath, M), get_finish_move(As, Moves, PathToEnd, Path)
    ;otherwise -> head(List, M), get_finish_move(As, Moves, PathToEnd, Path)
    ).

% Update the path of the agents when the solution is known. If the agent is already
% on the path to the solution, there is no need to update. If the agent has not moved
% no need to update. If the agent has moved, then modify the agent path to be the
% tail (all elements in list except the first) of the current agent path
update_path_finish([], Path, _, Path).

update_path_finish([A|Agents], Path, PathToEnd, NewPath) :-
    get_agent_element(A, Path, AgentPath),
    get_agent_position(A, AP),
    (member(AP, PathToEnd) -> update_path_finish(Agents, Path, PathToEnd, NewPath)
    ;otherwise ->
        (second_elm(AgentPath, AP) ->
            tail(AgentPath, NewAgentPath),
            replace_element(A, NewAgentPath, Path, NewAcc),
            update_path_finish(Agents, NewAcc, PathToEnd, NewPath)
        ;otherwise ->
            update_path_finish(Agents, Path, PathToEnd, NewPath)
        )
    ).

% Use the path to the solution to move all other agents to the solution until
% all agents have left the maze.
finish([], _, _) :- true.

finish(Agents, PathToEnd, Path) :-
    get_finish_move(Agents, Moves, PathToEnd, Path),
    agents_do_moves(Agents, Moves),
    update_path_finish(Agents, Path, PathToEnd, NewPath),
    (finish_achieved(Agents) -> my_agents(NewAgents), finish(NewAgents, PathToEnd, NewPath)
    ;otherwise -> finish(Agents, PathToEnd, NewPath)).

% Modified achieved function to test if agent is at the solution. If it is, make
% the agent leave the maze.
finish_achieved([]) :- false.

finish_achieved([A|Agents]) :-
    achieved(A), !,
    leave_maze(A)
    ;
    finish_achieved(Agents).

% Check if any agent has reached the end of the maze. If true, get the path of
% the agent that has succeeded and leave the maze. Then use the path to send all
% other agents to the end. This succeeds due to the nature of the maze which only
% has a single solution from the start point to the end point, meaning no further
% exploration is required to achieve the solution
maze_achieved([A|Agents], Path) :-
    achieved(A),
    get_agent_element(A, Path, AgentPath),
    leave_maze(A),
    my_agents(NewAgents),
    reverse(AgentPath, PathToEnd),
    finish(NewAgents, PathToEnd, Path)
    ;
    maze_achieved(Agents, Path).

achieved(A) :-
    ailp_grid_size(Size),
    get_agent_position(A, P),
    P = p(Size,Size).
