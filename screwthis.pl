% Solve the maze, aiming to get all the agents to p(N,N)
solve_maze :-
    my_agents(Agents),
    setup_visit(Agents, Visited),
    setup_tree(Agents, -, Tree),
    setup_sp(Agents, Visited, Tree, SP),
    setup_branch(Agents, Visited, SP, Branch),
    check_blocked(Agents, Visited, [], [], NewAgents, [], _),
    find_moves(NewAgents, Moves, Visited, Visited, Branch, Tree, SP, [], NewVisited, NewPath, NewBranch, NewTree, NewSavePoints, NewDeadEnds),
    agents_do_moves(NewAgents, Moves),
    solve_maze(Agents, NewVisited, NewPath, NewBranch, NewTree, NewSavePoints, NewDeadEnds).

solve_maze(Agents, Visited, Path) :-
    find_moves(Agents, Moves, Visited, Path, NewVisited, NewPath),
    agents_do_moves(Agents, Moves),
    %(achieved(Agents) -> true
    %;otherwise ->
    solve_maze(Agents, NewVisited, NewPath).
    %).

solve_maze(Agents, Visited, Path, Branch, Tree, SavePoints, DeadEnds) :-
    blocked(Agents, Visited, DeadEnds, [], NewAgents, [], OtherAgents),
    find_moves(NewAgents, Moves, Visited, Path, Branch, Tree, SavePoints, DeadEnds, NewVisited, NewPath, NewBranch, NewTree, NewSavePoints, NewDeadEnds),
    find_moves(OtherAgents, OtherMoves),
    append(NewAgents, OtherAgents, MoveAgents),
    append(Moves, OtherMoves, AgentMoves),
    agents_do_moves(MoveAgents, AgentMoves),
    write("New: "), write(NewDeadEnds), write("\n"),
    update_deadend(NewTree, NewDeadEnds, UpdatedDeadEnds),
    write("Upd: "), write(UpdatedDeadEnds), write("\n"),
    (achieved(Agents) -> true
    ;otherwise -> solve_maze(Agents, NewVisited, NewPath, NewBranch, NewTree, NewSavePoints, UpdatedDeadEnds)).

%%%%%%%%%%%%%%%% USEFUL PREDICATES %%%%%%%%%%%%%%%%%%
% Find a possible move for each agent
find_moves([],[]).
find_moves([A|As],[M|Moves]) :-
    findall(P,agent_adjacent(A,P,_),PosMoves),
    random_member(M,PosMoves),
    find_moves(As,Moves).

find_moves([], [], V, P, V, P).
find_moves([A|As],[M|Moves], Visited, Path, NewVisited, NewPath) :-
    get_list(A, Visited, AgentVisited),
    get_list(A, Path, AgentPath),
    findall(P,(agent_adjacent(A,P,empty), \+ member(P, AgentVisited)),PosMoves),
    length(AgentPath, PathLen), length(PosMoves, MoveLen),
    (PosMoves = [] ->
        (PathLen > 1 ->
            tail(AgentPath, Temp),
            head(Temp, X),
            (lookup_pos(X, empty) ->
                M = X, tail(AgentPath, T),
                replace_list(A, T, Path, UpPath),
                find_moves(As, Moves, Visited, UpPath, NewVisited, NewPath)
            ;otherwise -> M = [], find_moves(As, Moves, Visited, Path, NewVisited, NewPath))
        ;otherwise ->
            head(AgentPath, M),
            find_moves(As, Moves, Visited, Path, NewVisited, NewPath)
        )
    ;MoveLen is 1 -> head(PosMoves, M),
        replace_list(A, [M|AgentVisited], Visited, UpVisited),
        replace_list(A, [M|AgentPath], Path, UpPath),
        find_moves(As, Moves, UpVisited, UpPath, NewVisited, NewPath)
    ;otherwise -> head(PosMoves, M),
        replace_list(A, [M|AgentVisited], Visited, UpVisited),
        replace_list(A, [M|AgentPath], Path, UpPath),
        find_moves(As, Moves, UpVisited, UpPath, NewVisited, NewPath)
    ).

find_moves([], [], V, P, B, T, SP, D, V, P, B, T, SP, D).
find_moves([A|As], [M|Moves], Visited, Path, Branch, Tree, SavePoints, DeadEnds, V, P, B, T, SP, D) :-
    get_list(A, Visited, AgentVisited),
    get_list(A, Path, AgentPath),
    get_list(A, Branch, AgentBranch),
    get_list(A, SavePoints, AgentSavePoint),
    findall(P, (agent_adjacent(A, P, empty), \+ member(P, AgentVisited)), Empty),
    findall(P, (agent_adjacent(A, P, empty), \+ check_visited(P, Visited)), PosMoves),
    \+ PosMoves = [],
    length(Empty, NumberOfChoices),
    (NumberOfChoices < 2 ->
        head(PosMoves, M),
        replace_list(A, [M|AgentVisited], Visited, NewVisited),
        replace_list(A, [M|AgentPath], Path, NewPath),
        replace_list(A, [M|AgentBranch], Branch, NewBranch),
        find_moves(As, Moves, NewVisited, NewPath, NewBranch, Tree, SavePoints, DeadEnds, V, P, B, T, SP, D)
    ;otherwise ->
        get_agent_position(A, AP),
        head(PosMoves, M),
        replace_list(A, [M|AgentVisited], Visited, NewVisited),
        replace_list(A, [M|AgentPath], Path, NewPath),
        replace_list(A, [M], Branch, NewBranch),
        replace_list(A, [AP], SavePoints, NewSavePoints),
        head(AgentSavePoint, ASP),
        (find(ASP:_, Tree, SubTree) ->
            ins(AP:Empty, SubTree, NewSubTree),
            update_tree(SubTree, NewSubTree, Tree, NewTree),
            find_moves(As, Moves, NewVisited, NewPath, NewBranch, NewTree, NewSavePoints, DeadEnds, V, P, B, T, SP, D)
        ;otherwise ->
            ins(AP:Empty, Tree, NewTree),
            find_moves(As, Moves, NewVisited, NewPath, NewBranch, NewTree, NewSavePoints, DeadEnds, V, P, B, T, SP, D)
        )
    )
    ;
    get_list(A, Visited, AgentVisited),
    get_list(A, Path, AgentPath),
    get_list(A, Branch, AgentBranch),
    get_list(A, SavePoints, AgentSavePoint),
    findall(P, (agent_adjacent(A, P, empty), \+ member(P, AgentVisited)), Empty),
    findall(P, (agent_adjacent(A, P, empty), \+ member(P, AgentVisited), \+ member(P, DeadEnds)), OtherMoves),
    \+ OtherMoves = [],
    length(Empty, NumberOfChoices),
    (NumberOfChoices < 2 ->
        head(OtherMoves, M),
        replace_list(A, [M|AgentVisited], Visited, NewVisited),
        replace_list(A, [M|AgentPath], Path, NewPath),
        replace_list(A, [M|AgentBranch], Branch, NewBranch),
        find_moves(As, Moves, NewVisited, NewPath, NewBranch, Tree, SavePoints, DeadEnds, V, P, B, T, SP, D)
    ;otherwise ->
        get_agent_position(A, AP),
        head(OtherMoves, M),
        replace_list(A, [M|AgentVisited], Visited, NewVisited),
        replace_list(A, [M|AgentPath], Path, NewPath),
        replace_list(A, [M], Branch, NewBranch),
        replace_list(A, [AP], SavePoints, NewSavePoints),
        head(AgentSavePoint, ASP),
        (find(ASP:_, Tree, SubTree) ->
            ins(AP:Empty, SubTree, NewSubTree),
            update_tree(SubTree, NewSubTree, Tree, NewTree),
            find_moves(As, Moves, NewVisited, NewPath, NewBranch, NewTree, NewSavePoints, DeadEnds, V, P, B, T, SP, D)
        ;otherwise ->
            ins(AP:Empty, Tree, NewTree),
            find_moves(As, Moves, NewVisited, NewPath, NewBranch, NewTree, NewSavePoints, DeadEnds, V, P, B, T, SP, D)
        )
    )
    ;
    get_list(A, Path, AgentPath),
    get_list(A, Branch, AgentBranch),
    tail(AgentPath, Temp),
    head(Temp, M),
    tail(AgentPath, NewAgentPath),
    replace_list(A, NewAgentPath, Path, NewPath),
    (AgentBranch = [] ->
        find_moves(As, Moves, Visited, NewPath, Branch, Tree, SavePoints, DeadEnds, V, P, B, T, SP, D)
    ;otherwise ->
        replace_list(A, [], Branch, NewBranch),
        append(AgentBranch, DeadEnds, NewDeadEnds),
        find_moves(As, Moves, Visited, NewPath, NewBranch, Tree, SavePoints, NewDeadEnds, V, P, B, T, SP, D)
    ).

ins(I, -, t(I, -, -, -)) :- !.

ins(I:_, t(I:X, L, M, R), t(I:X, L, M, R)) :- !.

ins(I:_, t(X, t(I:_, CL, CM, CR), M, R), t(X, t(I:_, CL, CM, CR), M, R)) :- !.

ins(I:_, t(X, L, t(I:_, CL, CM, CR), R), t(X, L, t(I:_, CL, CM, CR), R)) :- !.

ins(I:_, t(X, L, M, t(I:_, CL, CM, CR)), t(X, L, M, t(I:_, CL, CM, CR))) :- !.

ins(I, t(X, L, M, R), t(Y, P, N, Q)) :-
    (L = - -> ins(I, L, U), (Y, P, N, Q) = (X, U, M, R)
    ;M = - -> ins(I, M, U), (Y, P, N, Q) = (X, L, U, R)
    ;R = - -> ins(I, R, U), (Y, P, N, Q) = (X, L, M, U)
    ;otherwise -> fail
    ).

find(X, t(X, L, M, R), t(X, L, M, R)) :- !.

find(I, t(_, L, M, R), T) :-
    find(I, L, T)
    ;
    find(I, M, T)
    ;
    find(I, R, T)
    ;
    fail.

find_parent(X, t(X, _, _, _), -) :- !.

find_parent(X, t(I, t(X, CL, CM, CR), M, R), t(I, t(X, CL, CM, CR), M, R)) :- !.

find_parent(X, t(I, L, t(X, CL, CM, CR), R), t(I, L, t(X, CL, CM, CR), R)) :- !.

find_parent(X, t(I, L, M, t(X, CL, CM, CR)), t(I, L, M, t(X, CL, CM, CR))) :- !.

find_parent(I, t(_, L, M, R), T) :-
    find_parent(I, L, T)
    ;
    find_parent(I, M, T)
    ;
    find_parent(I, R, T).

replace_value(I, X, t(I, L, M, R), t(X, L, M, R)) :- !.

replace_value(I, X, T, N) :-
    find_parent(I, T, t(V, L, M, R)),
    (find(I, L, t(I, CL, CM, CR)) -> !, N = t(V, t(X, CL, CM, CR), M, R)
    ;find(I, M, t(I, CL, CM, CR)) -> !, N = t(V, L, t(X, CL, CM, CR), R)
    ;find(I, R, t(I, CL, CM, CR)) -> !, N = t(V, L, M, t(X, CL, CM, CR))
    ;otherwise -> fail
    ).

delete_subtree(S, T, N) :-
    find(X, S, S),
    find_parent(X, T, t(V, L, M, R)),
    (S = L -> !, N = t(V, -, M, R)
    ;S = M -> !, N = t(V, L, -, R)
    ;S = R -> !, N = t(V, L, M, -)
    ;otherwise -> N = T
    ).

setup_tree([], Tree, Tree).

setup_tree([A|As], Tree, X) :-
    get_agent_position(A, AP),
    findall(P, agent_adjacent(A, P, empty), Empty),
    findall(P, (agent_adjacent(A, P, a(Y)), member(Y, As)), Agent),
    append(Agent, Empty, Options), length(Options, OptionsLen),
    (AP = p(1, 1), OptionsLen > 1 -> ins(AP:Options, Tree, NewTree), setup_tree(As, NewTree, X)
    ;OptionsLen > 2 -> ins(AP:Options, Tree, NewTree), setup_tree(As, NewTree, X)
    ;otherwise -> setup_tree(As, Tree, X)
    ).

update_tree(t(I, _, _, _), NST, Tree, NewTree) :-
    find_parent(I, Tree, P),
    (P = t(X, t(I, _, _, _), M, R) -> update_tree(P, t(X, NST, M, R), Tree, NewTree)
    ;P = t(X, L, t(I, _, _, _), R) -> update_tree(P, t(X, L, NST, R), Tree, NewTree)
    ;P = t(X, L, M, t(I, _, _, _)) -> update_tree(P, t(X, L, M, NST), Tree, NewTree)
    ;otherwise -> NewTree = NST
    ).

get_tree_value(-, -).

get_tree_value(t(I:_, _, _, _), I).

update_vals(_, [], _, Acc, Acc).

update_vals(I, [A|As], DeadEnds, Acc, NewDeadEnds) :-
    member(A, DeadEnds),
    solve_task_bfs(go(A), [I:[]], [], [I|Path]),
    append(Path, Acc, NewAcc),
    update_vals(I, As, DeadEnds, NewAcc, NewDeadEnds)
    ;
    update_vals(I, As, DeadEnds, Acc, NewDeadEnds).

update_deadend(-, Acc, Acc).

update_deadend(t(I:X, -, -, -), DeadEnds, NewDeadEnds) :-
    list_in_deadend(X, DeadEnds, [], X),
    append([I], DeadEnds, NewDeadEnds)
    ;
    NewDeadEnds = DeadEnds.

update_deadend(t(I:X, L, M, R), DeadEnds, NewDeadEnds) :-
    update_deadend(L, DeadEnds, LDeadEnds),
    update_deadend(M, DeadEnds, MDeadEnds),
    update_deadend(R, DeadEnds, RDeadEnds),
    append(LDeadEnds, DeadEnds, Temp),
    append(MDeadEnds, Temp, Temp2),
    append(RDeadEnds, Temp2, AllDeadEndsD),
    sort(AllDeadEndsD, AllDeadEnds),
    get_tree_value(L, LeftVal),
    get_tree_value(M, MidVal),
    get_tree_value(R, RightVal),
    update_vals(I, [LeftVal, MidVal, RightVal], AllDeadEnds, [], Temp3),
    append(Temp3, AllDeadEnds, FinalDeadEnds),
    (list_in_deadend(X, FinalDeadEnds, [], X) -> !, append([I], FinalDeadEnds, NewDeadEnds)
    ;otherwise -> !, NewDeadEnds = FinalDeadEnds).

list_in_deadend([], _, Acc, List) :-
    reverse(Acc, List).

list_in_deadend([X|Xs], DeadEnds, Acc, List) :-
    member(X, DeadEnds),
    list_in_deadend(Xs, DeadEnds, [X|Acc], List)
    ;
    list_in_deadend(Xs, DeadEnds, Acc, List).

setup_visit([], []).

setup_visit([A|Agents], [L|List]) :-
    get_agent_position(A, Pos),
    (Pos = p(1,1) -> L = A:[Pos],
      setup_visit(Agents, List)
    ;otherwise -> solve_task_bfs(go(p(1,1)), [Pos:[]], [], Path),
      reverse(Path, NewPath),
      L = A:NewPath,
      setup_visit(Agents, List)).

setup_sp([], _, _, []).

setup_sp([A|Agents], [A:_|Vs], -, [L|Lists]) :-
    L = A:[],
    setup_sp(Agents, Vs, -, Lists).

setup_sp([A|Agents], [A:V|Vs], t(X, L, M, R), [List|Lists]) :-
    get_left(X, ChoicePoint),
    get_right(X, Options),
    get_agent_position(A, AP),
    (AP = ChoicePoint ->
        List = A:[AP],
        setup_sp(Agents, Vs, t(X, L, M, R), Lists)
    ;check_member(V, Options) ->
        (setup_sp([A], [A:V], L, [A:[]]), setup_sp([A], [A:V], M, [A:[]]), setup_sp([A], [A:V], R, [A:[]]) ->
            List = A:[ChoicePoint],
            setup_sp(Agents, Vs, t(X, L, M, R), Lists)
        ;otherwise ->
            (setup_sp([A], [A:V], L, [A:Ans]), \+ Ans = [] ->
                List = A:Ans,
                setup_sp(Agents, Vs, t(X, L, M, R), Lists)
            ;setup_sp([A], [A:V], M, [A:Ans]), \+ Ans = [] ->
                List = A:Ans,
                setup_sp(Agents, Vs, t(X, L, M, R), Lists)
            ;setup_sp([A], [A:V], R, [A:Ans]), \+ Ans = [] ->
                List = A:Ans,
                setup_sp(Agents, Vs, t(X, L, M, R), Lists)
            ;otherwise -> fail
            )
        )
    ;otherwise ->
        List = A:[],
        setup_sp(Agents, Vs, t(X, L, M, R), Lists)
    ).

setup_branch([], [], [], []).

setup_branch([A|Agents], [A:V|Vs], [A:[]|SPs], [B|Branch]) :-
    B = A:V,
    setup_branch(Agents, Vs, SPs, Branch).

setup_branch([A|Agents], [A:V|Vs], [A:[SP]|SPs], [B|Branch]) :-
    get_agent_position(A, AP),
    (AP = SP ->
        B = A:[],
        setup_branch(Agents, Vs, SPs, Branch)
    ;otherwise ->
        skip_till_sp(SP, V, AgentBranch),
        B = A:AgentBranch,
        setup_branch(Agents, Vs, SPs, Branch)
    ).

skip_till_sp(SP, [SP|Xs], Xs).

skip_till_sp(SP, [_|Xs], List) :-
    skip_till_sp(SP, Xs, List).

check_member([], _) :- !, fail.

check_member([X|Xs], L) :-
    member(X, L), !
    ;
    check_member(Xs, L).

blocked([], _, _, Acc, Acc, OAcc, OAcc).

blocked([A|Agents], Visited, DeadEnds, Acc, New, OAcc, ONew) :-
    get_agent_position(A, AP),
    get_list(A, Visited, AgentVisited),
    findall(P, (agent_adjacent(A, P, empty), \+ member(P, AgentVisited)), Empty),
    (member(AP, DeadEnds) ->
        append([A], Acc, NewAcc),
        blocked(Agents, Visited, DeadEnds, NewAcc, New, OAcc, ONew)
    ;Empty = [], head(AgentVisited, AP) ->
        append([A], Acc, NewAcc),
        blocked(Agents, Visited, DeadEnds, NewAcc, New, OAcc, ONew)
    ;\+ Empty = [] ->
        append([A], Acc, NewAcc),
        blocked(Agents, Visited, DeadEnds, NewAcc, New, OAcc, ONew)
    ;otherwise ->
        append([A], OAcc, NewOAcc),
        blocked(Agents, Visited, DeadEnds, Acc, New, NewOAcc, ONew)
    ).

check_blocked([], _, _, Acc, Acc, OAcc, OAcc).

check_blocked([A|Agents], Visited, DeadEnds, Acc, New, OAcc, ONew) :-
    get_agent_position(A, AP),
    get_list(A, Visited, AgentVisited),
    findall(P, (agent_adjacent(A, P, empty), \+ member(P, AgentVisited)), Empty),
    (member(AP, DeadEnds) ->
        append([A], Acc, NewAcc),
        check_blocked(Agents, Visited, DeadEnds, NewAcc, New, OAcc, ONew)
    ;\+ Empty = [] ->
        append([A], Acc, NewAcc),
        check_blocked(Agents, Visited, DeadEnds, NewAcc, New, OAcc, ONew)
    ;otherwise ->
        append([A], OAcc, NewOAcc),
        check_blocked(Agents, Visited, DeadEnds, Acc, New, NewOAcc, ONew)
    ).

get_left(X:_, X).
get_right(_:Y, Y).

get_list(A, [L|Ls], AgentList) :-
    get_left(L, A), get_right(L, AgentList)
    ;
    get_list(A, Ls, AgentList).

replace_list(_, _, [], []).

replace_list(A, List, [L|Ls], [N|Ns]) :-
    get_left(L, A), N = A:List, replace_list(A, List, Ls, Ns)
    ;
    N = L, replace_list(A, List, Ls, Ns).

check_visited(_, []) :- false.

check_visited(Pos, [V|Visited]) :-
    get_right(V, AgentVisited), member(Pos, AgentVisited)
    ;
    check_visited(Pos, Visited).

achieved([]) :- false.

achieved([A|Agents]) :-
    achieved(A), leave_maze(A)
    ;
    achieved(Agents).

achieved(A) :-
    ailp_grid_size(Size),
    get_agent_position(A, P),
    P = p(Size,Size).
