:- dynamic find_identity/1.

% Accomplish a given Task and return the Cost
solve_task(Task,Cost) :-
    my_agent(A), get_agent_position(A, P),
    get_agent_energy(A, Energy),
    \+ Energy is 0,
    (Task = find(_) ->
        % Find path to object
        solve_task_astar(Task,[0:P:[]],[],[P|Path]), !,
        length(Path, ExpectedCost),
        % Check if expected path can be executed with current energy
        (ExpectedCost > Energy ->
            % Since length of path is greater than energy, attempt to go to charging station
            % before going to object. If no charging station is reachable from current position,
            % the task fails.
            get_reachable_points(P, Energy, Points),
            get_charging_station(Points, [], Stations), maplist(get_empty, Stations, TempPoints),
            flatten(TempPoints, StationPoints), goto_charging_station(A, P, StationPoints, Energy, NP),
            solve_task_astar(Task,[0:NP:[]],[],[NP|Path]), !,
            length(Path, Cost), get_agent_energy(A, NEnergy),
            (Cost is 0 -> say("Cannot complete task", A), !, fail
            ;Cost =< NEnergy -> agent_do_moves(A,Path)
            ;otherwise -> say("Cannot complete task", A), !, fail
            )
        ;otherwise ->
            % Since the cost of the path is less than agent energy, execute the path.
            agent_do_moves(A, Path),
            Cost = ExpectedCost
        )
    ;otherwise ->
        % Since the position of the node to go to is known, we can check if the Manhattan
        % distance between the agent position and destination is greater than the agent energy.
        % If the Manhattan distance is greater, we must first go to a charging station before
        % finding a path to the destination.
        viable(Task), get_task(TaskP, Task),
        check_blocked([TaskP:[]], []),
        get_distance(Task, P, Distance),
        (Distance > Energy ->
            get_reachable_points(P, Energy, Points),
            get_charging_station(Points, [], Stations),
            maplist(get_empty, Stations, TempPoints),
            flatten(TempPoints, StationPoints),
            goto_charging_station(A, P, StationPoints, Energy, NP),
            solve_task_astar(Task,[0:NP:[]],[],[NP|Path]), !,
            length(Path,Cost), get_agent_energy(A,NEnergy),
            (Cost is 0 -> say("Cannot complete task", A), !, fail
            ;Cost =< NEnergy -> agent_do_moves(A,Path)
            ;otherwise -> say("Cannot complete task", A), !, fail
            )
        ;otherwise ->
            % Since the Manhattan distance between the two is less than the agent energy,
            % we first find a path to the destination, and then verify if it is possible
            % to execute with our current energy. If it is not possible, we must go to a
            % charging station before going to the destination. If no charging stations
            % are reachable from the current location with the current energy, this fails
            solve_task_astar(Task,[0:P:[]],[],[P|Path]), !,
            length(Path,Cost),
            (Cost is 0 -> say("Cannot complete task", A), !, fail
            ;Cost =< Energy -> agent_do_moves(A,Path)
            ;otherwise ->
                  % Since cost of the path is greater than current energy, go to charging
                  % station and then go to destination.
                  get_reachable_points(P, Energy, Points),
                  get_charging_station(Points, [], Stations),
                  maplist(get_empty, Stations, TempPoints),
                  flatten(TempPoints, StationPoints),
                  goto_charging_station(A, P, StationPoints, Energy, NP),
                  solve_task_astar(Task,[0:NP:[]],[],[NP|NPath]), !,
                  length(NPath,NCost), get_agent_energy(A,NEnergy),
                  (NCost is 0 -> say("Cannot complete task", A), !, fail
                  ;NCost =< NEnergy -> agent_do_moves(A,NPath)
                  ;otherwise -> say("Cannot complete task", A), !, fail
                  )
            )
        )
    ).

% Calculate the path required to achieve a Task
solve_task_dfs(Task,[P|Ps],Path) :-
    achieved(Task,P), reverse([P|Ps],Path)
    ;
    map_adjacent(P,Q,empty), \+ member(Q,Ps),
    solve_task_dfs(Task,[Q,P|Ps],Path).

% Calculate the path required to achieve a Task using breadth-first search
solve_task_bfs(Task,[Pos:RPath|Queue],Visited,Path) :-
    achieved(Task, Pos), reverse([Pos|RPath], Path)
    ;
    children([Pos:RPath|Queue], Visited, Children),
    append(Queue, Children, NewQueue),
    solve_task_bfs(Task, NewQueue, [Pos|Visited], Path).

%
check_blocked([Pos:RPath|Queue], Visited) :-
    length(Visited, 10)
    ;
    children([Pos:RPath|Queue], Visited, Children),
    append(Queue, Children, NewQueue),
    check_blocked(NewQueue, [Pos|Visited]).

solve_task_astar(Task,[_:Pos:RPath|Queue],Visited,Path) :-
    achieved(Task, Pos), reverse([Pos|RPath], Path)
    ;
    Task=go(_), get_task(Destination, Task), \+ lookup_pos(Destination, empty);
    children([Pos:RPath|Queue], Visited, Children),
    (Task=find(_) -> % If task is find(), then perform breadth-first search (equivalent to A* with monotonic function returning
                     % distance between current position and start point)
      append(Queue, Children, NewQueue),
      solve_task_bfs(Task, NewQueue, [Pos|Visited], Path)
    ;otherwise ->    % Otherwise, perform A* search with sorting of agenda based on distance calculated
      maplist(distance(Task), Children, Dists),
      maplist(pack, Dists, Children, ChildrenDistance),
      sort(ChildrenDistance, SortedChildren),
      ord_union(Queue, SortedChildren, NewQueue),
      solve_task_astar(Task, NewQueue, [Pos|Visited], Path)
    ).

% True if the Task is achieved with the agent at Pos
achieved(Task,Pos) :-
    Task=find(Obj), map_adjacent(Pos,_,Obj)
    ;
    Task=go(Pos).

%%%%%%%%%%%%%%%%%%%%% HELPER FUNCTIONS %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find adjacent nodes to current position while excluding previously visited nodes and nodes already in queue
children([Node:Nodes|Queue], Visited, Children) :-
    findall(NP:[Node|Nodes], (map_adjacent(Node, NP, empty), \+ member(NP, Visited), \+ member(NP:_, Queue)), A),
    findall(NP:[Node|Nodes], (map_adjacent(Node, NP, a(_)), \+ member(NP, Visited), \+member(NP:_, Queue)), B),
    append(A, B, Children).

viable(go(P)) :-
    findall(X, map_adjacent(P, X, empty), Xs),
    \+ length(Xs, 0).

viable(find(_)) :- true.

get_distance(go(Target), Pos, Distance) :-
    map_distance(Pos, Target, Distance).

get_distance(find(_), _, 0).

distance(_, _:[], 0).

% Return distance from start to current node + distance from current node to target nodes
% Distance from current node to target node is 0 if the position of the target node is not known
distance(go(Target), Pos:RPath, Distance) :-
    ground(Target), length(RPath, Dist1), map_distance(Pos, Target, Dist2), (Distance is Dist1 + Dist2).

pack(X,Y,X:Y).

point(X,Y,p(X,Y)).

removeAll(_, [], []).
removeAll(X, [X|T], L):- removeAll(X, T, L), !.
removeAll(X, [H|T], [H|L]):- removeAll(X, T, L ).

% Generate a list of points that contains points with a distance of MaxDist from a point
% p(X, Y) in all 4 directions, i.e in North, South, East and West
gen(p(X,Y), MaxDist, Frontier) :-
    PosX is X+MaxDist, PosY is Y+MaxDist,
    NegX is X-MaxDist, NegY is Y-MaxDist,
    Frontier = [p(PosX, Y), p(X, PosY), p(NegX, Y), p(X, NegY)].

% Helper function to generate a list of numbers from X1 to X2. Since the library function
% only generates a list if X1 =< X2, this function is created to generate a list of elements
% from X1 to X2 when X1 > X2, while preserving the order of the arguments.
numlist_(X1, X2, List) :-
    (X1 =< X2 -> numlist(X1, X2, List)
    ;otherwise -> numlist(X2, X1, Temp), reverse(Temp, List)).

% Given 2 points p(X1, Y1) and p(X2, Y2), generates a list of points in a straight line
% from p(X1, Y1) to p(X2, Y2).
edge(p(X1,Y1), p(X2,Y2), Edges) :-
    numlist_(X1,X2,Xs), numlist_(Y1,Y2,Ys),
    maplist(point, Xs, Ys, Edges).

% Given 4 points, (P1, P2, P3, P4), generates a list of points that form a straight line
% from P1 to P2, P2 to P3, P3 to P4 and P4 to P1.
gen(Frontier, Edges) :-
    head(Frontier, H), tail(Frontier, T), append(T, [H], F),
    maplist(edge, Frontier, F, TempEdges), flatten(TempEdges, Edges).

% Given 2 points with the same X co-coordinate, generates a list of points that form a
% straight line from A to B.
l2r(p(X, Y1), p(X, Y2), Points) :-
    numlist_(Y1, Y2, Ys), maplist(point(X), Ys, Points).

% Given a list of co-ordinates such that the x-coordinate value in L[i] is equal to
% the x-coordinate value in R[size-i], returns a list of all points bounded by these
% co-ordinates.
all(L, R, Points) :-
    reverse(R, RevR),
    maplist(l2r, L, RevR, Temp), flatten(Temp, AllPs),
    remove_out_of_bounds(AllPs, [], Points).

% Removes all co-ordinates that are not in the grid from a given list.
remove_out_of_bounds([p(X,Y)], Acc, Points) :-
    ailp_grid_size(Size),
    (between(1, Size, X), between(1, Size, Y) -> append([p(X,Y)], Acc, Points)
    ;otherwise -> append([], Acc, Points)).

% Removes all co-ordinates that are not in the grid from a given list.
remove_out_of_bounds([p(X,Y)|Ps], Acc, Points) :-
    ailp_grid_size(Size),
    (between(1, Size, X), between(1, Size, Y) -> append([p(X,Y)], Acc, NewAcc),
    remove_out_of_bounds(Ps, NewAcc, Points)
    ;otherwise -> remove_out_of_bounds(Ps, Acc, Points)).

% Get a list of all points that are within reach of a position p(X, Y) with the
% given energy.
get_reachable_points(p(X,Y), Energy, Ans) :-
    ailp_grid_size(Size),
    (Energy > Size * 2 -> MaxDist is Size * 2
    ;otherwise -> MaxDist is Size),
    gen(p(X,Y), MaxDist, Frontier), gen(Frontier, Edges),
    div(Edges, L, R), all(L, R, Points), sort(Points, UPoints),
    removeAll(p(X,Y), UPoints, Ans).

% Check if a charging station is present in the list of points.
get_charging_station([P|[]], Acc, Stations) :-
    lookup_pos(P, Obj),
    (Obj = c(_) -> append([P], Acc, NewAcc), findall(Pos, map_adjacent(P, Pos, c(_)), Cs), append(Cs, NewAcc, Stations)
    ;otherwise -> findall(Pos, map_adjacent(P, Pos, c(_)), Cs), append(Cs, Acc, NewAcc), sort(NewAcc, Stations)).

% Check if a charging station is present in the list of points.
get_charging_station([P|Ps], Acc, Stations) :-
    lookup_pos(P, Obj),
    (Obj = c(_) -> append([P], Acc, TempAcc), findall(Pos, map_adjacent(P, Pos, c(_)), Cs), append(Cs, TempAcc, NewAcc)
    ;otherwise -> findall(Pos, map_adjacent(P, Pos, c(_)), Cs), append(Cs, Acc, NewAcc)),
    get_charging_station(Ps, NewAcc, Stations).

get_empty(P, Empty) :-
    findall(E, map_adjacent(P, E, empty), Empty).

get_task(p(X,Y), go(p(X,Y))).

head([], []).

head([H|_], H).

tail([_|T], T).

div(L, A, B) :-
    split(L, L, A, B).

split(B, [], [], B).

split([H|T], [_, _|T1], [H | T2], B) :-
    split(T, T1, T2, B).

% Given a list of charging stations with Manhattan distance less than or equal to
% the energy from the agent's position, go to the first charging station in the list
% that can be reached with the agent's current energy and recharge the agent's energy.
% This does not always go to the closest charging station.
goto_charging_station(_, P, [], _, P).

goto_charging_station(A, P, Ps, Energy, NP) :-
    head(Ps, Goal), get_task(Goal, Task),
    (viable(Task), check_blocked([Goal:[]], []) ->
    solve_task_astar(Task, [0:P:[]], [], [P|Path]), !,
    length(Path, Cost),
    (Cost =< Energy -> agent_do_moves(A, Path), last(Path, NP), map_adjacent(NP, Charge, c(_)),
    lookup_pos(Charge, StationObj), agent_topup_energy(A, StationObj)
    ;otherwise -> tail(Ps, NPs), goto_charging_station(A, P, NPs, Energy, NP)
    )
    ;otherwise -> tail(Ps, NPs), goto_charging_station(A, P, NPs, Energy, NP)).
